package com.example.demo.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.example.demo.entity.DemoDO;
import com.example.demo.mapper.DemoMapper;
import com.example.demo.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author kuangzhenhui
 * @version 2020/11/9
 */
@RestController
public class DemoController {

    @Autowired
    private DemoMapper demoMapper;
    @Autowired
    private RedisUtil redisUtil;

    @PostMapping("/hello")
    public Map<String, String> hello(@RequestParam("id1") String id1, @RequestParam("id2") String id2) {
        Map<String, String> result = new HashMap<>();
        //redis
        String redisValue = (String) redisUtil.get(id1 + id2);
        if (StringUtils.isNotBlank(redisValue)) {
            result.put("userId", redisValue);
            return result;
        }

        QueryWrapper<DemoDO> wrapper = new QueryWrapper<>();
        wrapper.eq("id1", id1);
        wrapper.eq("id2", id2);
        List<DemoDO> existList = demoMapper.selectList(wrapper);
        if (CollectionUtils.isEmpty(existList)) {
            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
            DemoDO demo = new DemoDO().setId1(id1).setId2(id2).setUserId("");
            demoMapper.insert(demo);
            result.put("userId", uuid);
        } else {
            result.put("userId", existList.get(0).getUserId());
        }
        redisUtil.setEx(id1 + id2, result.get("userId"), 1, TimeUnit.HOURS);
        return result;
    }


}

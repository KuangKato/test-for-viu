package com.example.demo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author kuangzhenhui
 * @version 2020/11/9
 */
@Data
@TableName("demo")
@Accessors(chain = true)
public class DemoDO {

    private Long id;

    private String id1;

    private String id2;

    private String userId;

}

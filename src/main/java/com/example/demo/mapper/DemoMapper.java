package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.entity.DemoDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author kuangzhenhui
 * @version 2020/11/9
 */
@Mapper
public interface DemoMapper extends BaseMapper<DemoDO> {
}

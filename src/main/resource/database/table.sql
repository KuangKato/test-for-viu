CREATE TABLE `demo` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `id1` varchar(32) DEFAULT NULL COMMENT 'id1',
  `id2` varchar(32) DEFAULT NULL COMMENT 'id2',
  `user_id` varchar(64) DEFAULT NULL COMMENT 'userId',
  PRIMARY KEY (`id`),
  KEY `idx_by_id` (`id1`,`id2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;